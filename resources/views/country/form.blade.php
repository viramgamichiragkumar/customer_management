@extends('admin.layouts.master')
@section('title')
    {{ $moduleName ?? '' }}
@endsection

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Add {{ $moduleName ?? '' }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('country.index') }}">{{ $moduleName ?? '' }}</a></li>
                    <li class="breadcrumb-item active">Add</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Create New {{ $moduleName ?? '' }}</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form action="{{ route('country.store') }}" id="country_form" method="post" class="form-horizontal form-label-left" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="country_name">
                                            Country <span class="requride_cls">*</span>
                                        </label>
                                        <input type="text" name="country_name" class="form-control input-sm" id="country_name" placeholder="Country Name" value="{{ old('country_name') }}">
                                        @if ($errors->has('country_name'))
                                            <span class="requride_cls"><strong>{{ $errors->first('country_name') }}</strong></span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <center>
                                    <a href=" {{ route('country.index') }}" class="btn btn-default">Cancel</a>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')

<script>
    $(document).ready(function(){
        $("#country_form").validate({
            rules: {
                country_name: {
                    required: true,
                    remote: {
                        type: "POST",
                        url : "{{ route('country.checkCountryName') }}",
                        data: {
                            country_name: function(){
                                return $("#country_name").val();
                            }
                        },
                    },
                },
            },
            messages: {
                country_name: {
                    required: "Country Name Is Required.",
                    remote: "Country Name Is Already Exists."
                },
            },
            submitHandler: function(form) {
                $(':input[type="submit"]').prop('disabled', true);
                form.submit();
            }
        });
    });
</script>

@endsection

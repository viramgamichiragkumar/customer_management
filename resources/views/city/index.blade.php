@extends('admin.layouts.master')
@section('title')
    {{ $moduleName ?? '' }}
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">{{ $moduleName ?? '' }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                        <li class="breadcrumb-item active">{{ $moduleName ?? '' }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{ $moduleName }} Details</h3>
                <div class="card-tools">
                    <div class="btn-group">
                        <a href="{{ route('city.create') }}" class="btn btn-primary btn-sm"><i
                                class="fa fa-plus"></i>
                            New</a>
                    </div>
                </div>
            </div>
            <div class="card-body  table-responsive">
                <table class="datatable  table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="10%">Sr No.</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country</th>
                            <th>State Code</th>
                            <th>Modified By</th>
                            <th>Modified On</th>
                            <th>Creted By</th>
                            <th>Creted On</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">

            </div>

            <!-- /.card-footer-->

        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection


@section('script')
    <script>
        $(document).ready(function() {

            @if (Session::has('message'))
                Swal.fire(
                '{{ $moduleName }}',
                '{!! session('message') !!}',
                'success'
                )
            @endif

            @if (Session::has('failmessage'))
                Swal.fire(
                '{{ $moduleName }}',
                '{!! session('failmessage') !!}',
                'error'
                )
            @endif


            $('body').on('click', '.delete', function(e) {
                let delId = $(this).data('id');
                if (delId != '') {
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "{{ route('city.delete') }}",
                                type: "POST",
                                data: {
                                    id: delId
                                },
                                success: function(response) {
                                    if (response) {
                                        location.reload();
                                    }
                                },
                            });

                        }
                    });
                }

            });

            var datatable = $('.datatable').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                ajax: {
                    "url": "{{ route('city.getData') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data": {
                        is_active: function() {
                            return $("#is_active").val();
                        },
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'country.country',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'state.name',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'state.state_code'
                    },
                    {
                        data: 'modified_by.name'
                    },
                    {
                        data: 'modified_on'
                    },
                    {
                        data: 'created_by.name'
                    },
                    {
                        data: 'created_on'
                    },
                    {
                        data: 'action',orderable:false,searchable:false
                    },

                ],
            });

        });

        $(document).on('click', '#activate', function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            console.log(linkURL);
            Swal.fire({
                title: 'Are you sure want to Activate?',
                text: "As that can be undone by doing reverse.",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = linkURL;
                }
            });
        });

        $(document).on('click', '#deactivate', function(e) {
            e.preventDefault();
            var linkURL = $(this).attr("href");
            console.log(linkURL);
            Swal.fire({
                title: 'Are you sure want to De-Activate?',
                text: "As that can be undone by doing reverse.",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = linkURL;
                }
            });
        });
    </script>
@endsection

@extends('admin.layouts.master')
@section('title')
{{ $moduleName ?? '' }}
@endsection

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ $moduleName ?? '' }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                    <li class="breadcrumb-item active">{{ $moduleName ?? '' }}</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-12 col-lg order-1 order-lg-0">
            <div class="card mb-5">
                <div class="card-header">
                    <h3 class="card-title">{{ $moduleName }} Create</h3>
                    <div class="card-tools">
                    </div>
                </div>
                <div class="card-body table-responsive">
                    <form action="{{ route('city.store') }}" method="POST" enctype="multipart/form-data" id="form">
                        @csrf()
                        <div class="row g-3">
                            <div class="col-md-4 mb-3 col-sm-12">
                                <label class="form-label">Name <span class="requride_cls">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name"
                                    value="{{ old('name') }}" required />
                                @error('name')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3 col-sm-12">
                                <label for="board">State <span class="requride_cls">*</span></label>
                                <td>
                                    <select class="select2 select2bs4 form-control" id="state"
                                    name="state" required>
                                    <option value="">Select State</option>
                                        @foreach ($states as $state)
                                            <option value="{{ $state->id }}">{{ $state->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                @error('state')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3 col-sm-12">
                                <label class="form-label">State Code <span class="requride_cls">*</span></label>
                                <input type="text" class="form-control" name="state_code" readonly id="state_code" placeholder="State code"
                                    value="{{ old('state_code') }}" />
                                @error('state_code')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-4 mb-3 col-sm-12">
                                <label class="form-label">Country <span class="requride_cls">*</span></label>
                                <input type="text" class="form-control" name="country" readonly id="country" placeholder="Country"
                                    value="{{ old('country') }}" />
                                <input type="hidden" class="form-control" name="country_id" readonly id="country_id" value="{{ old('country_id') }}" />
                                @error('country')
                                    <span class="error">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                        </div>

                        <center class="mt-4">
                            <button class="btn btn-icon btn-icon-end btn-primary m-1" type="submit" id="submit">
                                Submit
                            </button>
                            <a href="{{ url()->previous() }}" class="btn btn-default m-1" onclick="history.back()">Cancel</a>
                        </center>
                </div>
            </div>

            </form>
        </div>
    </div>
    </div>
</section>

@endsection

@section('script')
    <script>
    $('#state').on('change',function(){
        $.ajax({
            url:"{{ route('city.GetState') }}",
            method:"post",
            data:{
                id:$(this).val(),
                '_token':'{{ csrf_token() }}',
            },
            success:function(res){
                res = $.parseJSON(res);
                $('#country').val('');
                $('#state_code').val('');
                if(res.length != 0){
                    $('#country').val(res['country']['country']);
                    $('#country_id').val(res['country']['id']);
                    $('#state_code').val(res['state_code']);
                }
            }
        });
    });

    $('#form').validate({
        rules: {
            name: {
                required: true,
            },
        },
        messages: {
            name: {
                required: "Name Is Required.",
            },
        },
        errorPlacement: function(error, element) {
            error.css('color', 'red').appendTo(element.parent("div"));
        },
        submitHandler: function(form) {
            form.submit();
            $(':input[type="submit"]').prop('disabled', true);
        }
    });

    </script>
@endsection

 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="{{ route('home') }}" class="brand-link">
         <img src="{{ asset(env('APP_LOGO')) }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
         <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
     </a>

     <!-- Sidebar -->
     <div class="sidebar">
         <!-- Sidebar Menu -->
         <nav class="mt-2">
             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                     <li class="nav-item">
                         <a href="{{ route('country.index') }}"
                             class="nav-link {{ \Request::segment(2) == 'country' ? 'active' : '' }}">
                             <i class="nav-icon fas fa-circle"></i>
                             <p>Country</p>
                         </a>
                     </li>


                     <li class="nav-item">
                         <a href="{{ route('state.index') }}"
                             class="nav-link {{ \Request::segment(2) == 'state' ? 'active' : '' }}">
                             <i class="nav-icon fas fa-circle"></i>
                             <p>State</p>
                         </a>
                     </li>
                     <li class="nav-item">
                         <a href="{{ route('city.index') }}"
                             class="nav-link {{ \Request::segment(2) == 'city' ? 'active' : '' }}">
                             <i class="nav-icon fas fa-circle"></i>
                             <p>City</p>
                         </a>
                     </li>

             </ul>
         </nav>
     </div>
 </aside>

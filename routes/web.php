<?php

use App\Http\Controllers\CityController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StateController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', function () {
        return redirect()->route('country.index');
    });

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::group(['prefix' => 'country'], function () {
        Route::get('/', [CountryController::class, 'index'])->name('country.index');
        Route::get('/getCountryData', [CountryController::class, 'getCountryData'])->name('country.getCountryData');
        Route::get('/create', [CountryController::class, 'create'])->name('country.create');
        Route::post('/store', [CountryController::class, 'store'])->name('country.store');
        Route::get('/edit/{id}', [CountryController::class, 'edit'])->name('country.edit');
        Route::put('/{id}', [CountryController::class, 'update'])->name('country.update');
        Route::get('/delete/{id}', [CountryController::class, 'delete'])->name('country.delete');
        Route::post('/checkCountryName', [CountryController::class, 'checkCountryName'])->name('country.checkCountryName');
    });

    Route::prefix('state')->group(function () {
        Route::get('/', [StateController::class, 'index'])->name('state.index');
        Route::get('/getData', [StateController::class, 'getData'])->name('state.getData');
        Route::get('/create', [StateController::class, 'create'])->name('state.create');
        Route::post('/store', [StateController::class, 'store'])->name('state.store');
        Route::get('/edit/{id}', [StateController::class, 'edit'])->name('state.edit');
        Route::post('/update/{id}', [StateController::class, 'update'])->name('state.update');
        Route::post('/delete', [StateController::class, 'delete'])->name('state.delete');
    });

    Route::prefix('city')->group(function () {
        Route::get('/', [CityController::class, 'index'])->name('city.index');
        Route::get('/getData', [CityController::class, 'getData'])->name('city.getData');
        Route::get('/create', [CityController::class, 'create'])->name('city.create');
        Route::post('/store', [CityController::class, 'store'])->name('city.store');
        Route::get('/edit/{id}', [CityController::class, 'edit'])->name('city.edit');
        Route::post('/update/{id}', [CityController::class, 'update'])->name('city.update');
        Route::post('/delete', [CityController::class, 'delete'])->name('city.delete');
        Route::post('/GetState', [CityController::class, 'GetState'])->name('city.GetState');
    });

    Route::prefix('orders')->group(function () {
        Route::get('/', [App\Http\Controllers\Admin\OrdersController::class, 'index'])->name('orders.index');
        Route::post('getReceivedOrdersData', [App\Http\Controllers\Admin\OrdersController::class, 'getReceivedOrdersData'])->name('orders.getReceivedOrdersData');
        Route::post('getShippedOrdersData', [App\Http\Controllers\Admin\OrdersController::class, 'getShippedOrdersData'])->name('orders.getShippedOrdersData');
        Route::post('getDeliveredOrdersData', [App\Http\Controllers\Admin\OrdersController::class, 'getDeliveredOrdersData'])->name('orders.getDeliveredOrdersData');
        Route::post('getCancelledOrdersData', [App\Http\Controllers\Admin\OrdersController::class, 'getCancelledOrdersData'])->name('orders.getCancelledOrdersData');
        Route::post('getRejectedOrdersData', [App\Http\Controllers\Admin\OrdersController::class, 'getRejectedOrdersData'])->name('orders.getRejectedOrdersData');
        Route::get('show/{id}', [App\Http\Controllers\Admin\OrdersController::class, 'show'])->name('orders.show');
        Route::post('state/change', [App\Http\Controllers\Admin\OrdersController::class, 'stateChange'])->name('orders.state.change');
    });

    Route::prefix('return-orders')->group(function () {
        Route::get('/', [ReturnController::class, 'index'])->name('return.orders.index');
        Route::post('getRequestRegisteredData', [ReturnController::class, 'getRequestRegisteredData'])->name('return.orders.getRequestRegisteredData');
        Route::post('getRequestAcceptedData', [ReturnController::class, 'getRequestAcceptedData'])->name('return.orders.getRequestAcceptedData');
        Route::post('getRequestRejectedData', [ReturnController::class, 'getRequestRejectedData'])->name('return.orders.getRequestRejectedData');
        Route::post('getReturnTakenData', [ReturnController::class, 'getReturnTakenData'])->name('return.orders.getReturnTakenData');
        Route::post('getReturnAcceptedData', [ReturnController::class, 'getReturnAcceptedData'])->name('return.orders.getReturnAcceptedData');
        Route::post('getReturnRejectedData', [ReturnController::class, 'getReturnRejectedData'])->name('return.orders.getReturnRejectedData');
        Route::post('getCashbackGivenData', [ReturnController::class, 'getCashbackGivenData'])->name('return.orders.getCashbackGivenData');
        Route::post('getReplacementGivenData', [ReturnController::class, 'getReplacementGivenData'])->name('return.orders.getReplacementGivenData');
        Route::post('state/change', [ReturnController::class, 'stateChange'])->name('return.orders.state.change')->middleware('permission:create.returnorders');
    });

    Route::group(['prefix' => 'category'], function(){
        Route::get('/', [App\Http\Controllers\Admin\CategoryController::class, 'index'])->name('category.index');
        Route::get('/getCategoryData', [App\Http\Controllers\Admin\CategoryController::class, 'getCategoryData'])->name('category.getCategoryData');
        Route::get('/create', [App\Http\Controllers\Admin\CategoryController::class, 'create'])->name('category.create');
        Route::post('/store', [App\Http\Controllers\Admin\CategoryController::class, 'store'])->name('category.store');
        Route::get('/edit/{id}', [App\Http\Controllers\Admin\CategoryController::class,'edit'])->name('category.edit');
        Route::put('/{id}', [App\Http\Controllers\Admin\CategoryController::class, 'update'])->name('category.update');
        Route::get('/delete/{id}',[App\Http\Controllers\Admin\CategoryController::class, 'delete'])->name('category.delete');
        Route::post('/checkName',[App\Http\Controllers\Admin\CategoryController::class, 'checkName'])->name('category.checkName');
        Route::get('/categoryActiveInactive/{id}',[App\Http\Controllers\Admin\CategoryController::class, 'categoryActiveInactive'])->name('category.activeInactive');
    });

    /* Routes For ContactUs */
    Route::get('contact_us',[App\Http\Controllers\Admin\ContactUsController::class, 'index'])->name('contact_us.index');
    Route::get('/getContactUsData',[App\Http\Controllers\Admin\ContactUsController::class, 'getContactUsData'])->name('contact_us.getContactUsData');

    /* Routes Fro CMS Pages */
    Route::group(['prefix' => 'cms_page'], function() {
        Route::get('/', [App\Http\Controllers\Admin\CMSPageController::class, 'index'])->name('cms_page.index');
        Route::get('/getCmsPageData', [App\Http\Controllers\Admin\CMSPageController::class, 'getCmsPageData'])->name('cms_page.getCmsPageData');
        Route::get('/create', [App\Http\Controllers\Admin\CMSPageController::class, 'create'])->name('cms_page.create');
        Route::post('/store', [App\Http\Controllers\Admin\CMSPageController::class, 'store'])->name('cms_page.store');
        Route::get('/edit/{id}', [App\Http\Controllers\Admin\CMSPageController::class, 'edit'])->name('cms_page.edit');
        Route::put('/{id}', [App\Http\Controllers\Admin\CMSPageController::class, 'update'])->name('cms_page.update');
        Route::get('/delete/{id}', [App\Http\Controllers\Admin\CMSPageController::class, 'delete'])->name('cms_page.delete');
    });

    Route::prefix('banner')->group(function () {
        Route::get('/',[HomeBanner::class, 'index'])->name('banner.index');
        Route::get('/getBannerData',[HomeBanner::class, 'getBannerData'])->name('banner.getBannerData');
        Route::get('/create', [HomeBanner::class, 'create'])->name('banner.create');
        Route::post('/store', [HomeBanner::class, 'store'])->name('banner.store');
        Route::get('/view/{id}', [HomeBanner::class, 'view'])->name('banner.view');
        Route::get('/delete/{id}', [HomeBanner::class, 'delete'])->name('banner.delete');
        Route::get('/activeInactive/{id}',[HomeBanner::class, 'activeInactive'])->name('banner.activeInactive');
    });
});

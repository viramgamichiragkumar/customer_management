<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\City;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class CityController extends Controller
{
    public $moduleName = 'City';
    public $route = 'city';
    public $view = 'city';


    public function index()
    {
        $moduleName = $this->moduleName;
        $states = State::get();
        return view($this->view . '/index', compact('moduleName', 'states'));
    }

    public function create()
    {
        $moduleName = $this->moduleName;
        $states = State::get();
        return view($this->view . '/form', compact('moduleName', 'states'));
    }

    public function store(Request $request)
    {
        $city = City::create([
            'name'  => ucfirst(trim($request->name)),
            'state_id'  => $request->state ?? '',
            'country_id'  => $request->country_id ?? '',
        ]);

        Helper::successMsg('insert', $this->moduleName);
        return redirect(route($this->route . '.index'));
    }


    public function getData(Request $request)
    {
        $data = City::with(['state','country','modifiedBy','createdBy'])->select();

        return DataTables::eloquent($data)
            ->addColumn('action', function ($row) {
                $editUrl = route($this->route . '.edit', encrypt($row->id));
                $deleteId = encrypt($row->id);

                $action = '';
                $action .= "<a href='" . $editUrl . "' class='btn btn-warning btn-xs'><i class='fas fa-pencil-alt'></i> Edit</a>";
                $action .= " <a id='delete' href='#' data-id='" . $deleteId . "' class='btn btn-danger btn-xs delete'><i class='fa fa-trash'></i> Delete</a>";

                return $action;
            })
            ->editColumn('created_on', function ($row) {
                return date('d-m-Y',strtotime($row->created_at));
            })
            ->editColumn('modified_on', function ($row) {
                if ($row->modified_on != null) {
                    return date('d-m-Y',strtotime($row->modified_on));
                } else {
                    return '----';
                }
            })

            ->rawColumns(['action','created_on','modified_on'])
            ->addIndexColumn()
            ->make(true);
    }

    public function edit($id)
    {
        $moduleName = $this->moduleName;
        $states = State::get();
        $city = City::find(decrypt($id));
        return view($this->view . '/_form', compact('moduleName', 'states','city'));
    }

    public function update(Request $request,$id)
    {
        $city = City::where('id',decrypt($id))->first();

        $city->update([
            'name'  => ucfirst(trim($request->name)),
            'state_id'  => $request->state ?? '',
            'country_id'  => $request->country_id ?? '',
            'modified_by' => Auth::user()->id,
            'modified_on' => date('Y-m-d'),
        ]);


        Helper::successMsg('update', $this->moduleName);

        return redirect(route($this->route . '.index'));
    }

    public function delete(Request $request)
    {
        $res = false;
        $city = City::where('id', decrypt($request->id))->first();
        $res = $city->delete();

        if ($res) {
            Helper::successMsg('delete', $this->moduleName);
        } else {
            Helper::failarMsg('custom', 'There might be an Error!');
        }
        return response()->json($res);
    }

    public function GetState(Request $request){
        if(isset($request->id) && !empty($request->id)){
            $res = State::with(['country'])->find($request->id);
            return json_encode($res);
        }else{
            $res =[];
            return json_encode($res);
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;

class CountryController extends Controller
{
    public $moduleName = "Country";
    public $route = "/country";
    public $view = "/country";

    public function index()
    {
        $moduleName = $this->moduleName;
        return view($this->view . "/index", compact('moduleName'));
    }

    public function getCountryData()
    {
        $country = Country::with(['modifiedBy','createdBy'])->select();

        $datatables = datatables()->eloquent($country)
            ->addColumn('action', function ($row) {
                $editUrl = route('country.edit', encrypt($row->id));
                $deleteUrl = route('country.delete', encrypt($row->id));
                $action = '';

                $action .= "<a href='" . $editUrl . "' class='btn btn-warning btn-xs'><i class='fas fa-pencil-alt'></i> Edit</a>";
                $action .= " <a id='delete' href='" . $deleteUrl . "' class='btn btn-danger btn-xs delete'><i class='fa fa-trash'></i> Delete</a>";

                return $action;
            })
            ->editColumn('created_on', function ($row) {
                return date('d-m-Y',strtotime($row->created_at));
            })
            ->editColumn('modified_on', function ($row) {
                if ($row->modified_on != null) {
                    return date('d-m-Y',strtotime($row->modified_on));
                } else {
                    return '----';
                }
            })
            ->rawColumns(['action', 'modified_on', 'created_on'])
            ->addIndexColumn()
            ->make(true);

        return $datatables;
    }

    public function create()
    {
        $moduleName = $this->moduleName;
        return view($this->view . "/form", compact('moduleName'));
    }

    public function store(Request $request)
    {
        $validate = $request->validate([
            'country_name'  => 'required|unique:countries,country',
        ]);

        if ($validate) {
            DB::beginTransaction();

            $country = Country::create([
                'country'        => $request->country_name,
            ]);

            DB::commit();
        }

        Helper::successMsg('insert', $this->moduleName);
        return redirect($this->route);
    }

    public function edit($id)
    {
        $moduleName = $this->moduleName;
        $country = Country::find(decrypt($id));
        return view($this->view . '/_form', compact('moduleName', 'country'));
    }

    public function update(Request $request, $id)
    {
        $validate = $request->validate([
            'country_name'  => 'required|unique:countries,country,' . $request->id,
        ]);

        if ($validate) {
            DB::beginTransaction();

            $country = Country::find(decrypt($id));

            $country->update([
                'country' => $request->country_name,
                'modified_by' => Auth::user()->id,
                'modified_on' => date('Y-m-d'),
            ]);

            DB::commit();
        }

        Helper::successMsg('update', $this->moduleName);
        return redirect($this->route);
    }

    public function delete($id)
    {
        $country = Country::find(decrypt($id));
        DB::beginTransaction();
        if (isset($country->image)) {
            try {
                unlink(storage_path('/app/country/' . $country->image));
            } catch (Throwable $e) {
            }
        }
        $country->delete();
        DB::commit();

        Helper::successMsg('delete', $this->moduleName);
        return redirect($this->route);
    }

    public function checkCountryName(Request $request)
    {
        if (!isset($request->id)) {
            $country = Country::where('country', $request->country_name)->count();
        } else {
            $country = Country::where('country', $request->country_name)->where('id', '!=', $request->id)->count();
        }

        if ($country > 0) {
            return response()->json(false);
        } else {
            return response()->json(true);
        }
    }

}

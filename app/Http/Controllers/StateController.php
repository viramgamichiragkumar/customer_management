<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class StateController extends Controller
{
    public $moduleName = 'State';
    public $route = 'state';
    public $view = 'state';


    public function index()
    {
        $moduleName = $this->moduleName;
        $countries = Country::get();
        return view($this->view . '/index', compact('moduleName', 'countries'));
    }

    public function create()
    {
        $moduleName = $this->moduleName;
        $countries = Country::get();
        return view($this->view . '/form', compact('moduleName', 'countries'));
    }

    public function store(Request $request)
    {
        $state = State::create([
            'name'  => ucfirst(trim($request->name)),
            'country_id'  => $request->country,
            'state_code'  => $request->state_code ?? '',
        ]);

        Helper::successMsg('insert', $this->moduleName);
        return redirect(route($this->route . '.index'));
    }


    public function getData(Request $request)
    {
        $data = State::with(['country','modifiedBy','createdBy'])->select();

        return DataTables::eloquent($data)
            ->addColumn('action', function ($row) {
                $editUrl = route($this->route . '.edit', encrypt($row->id));
                $deleteId = encrypt($row->id);

                $action = '';
                $action .= "<a href='" . $editUrl . "' class='btn btn-warning btn-xs'><i class='fas fa-pencil-alt'></i> Edit</a>";
                $action .= " <a id='delete' href='#' data-id='" . $deleteId . "' class='btn btn-danger btn-xs delete'><i class='fa fa-trash'></i> Delete</a>";

                return $action;
            })
            ->editColumn('created_on', function ($row) {
                return date('d-m-Y',strtotime($row->created_at));
            })
            ->editColumn('modified_on', function ($row) {
                if ($row->modified_on != null) {
                    return date('d-m-Y',strtotime($row->modified_on));
                } else {
                    return '----';
                }
            })

            ->rawColumns(['action','created_on','modified_on'])
            ->addIndexColumn()
            ->make(true);
    }

    public function edit($id)
    {
        $moduleName = $this->moduleName;
        $countries = Country::get();
        $state = State::find(decrypt($id));
        return view($this->view . '/_form', compact('moduleName', 'countries','state'));
    }

    public function update(Request $request,$id)
    {
        $state = State::where('id',decrypt($id))->first();


        $state->update([
            'name'  => ucfirst(trim($request->name)),
            'country_id'  => $request->country,
            'state_code'  => $request->state_code ?? '',
            'modified_by' => Auth::user()->id,
            'modified_on' => date('Y-m-d'),
        ]);


        Helper::successMsg('update', $this->moduleName);

        return redirect(route($this->route . '.index'));
    }

    public function delete(Request $request)
    {
        $res = false;
        $state = State::where('id', decrypt($request->id))->first();
        $res = $state->delete();

        if ($res) {
            Helper::successMsg('delete', $this->moduleName);
        } else {
            Helper::failarMsg('custom', 'There might be an Error!');
        }
        return response()->json($res);
    }

}

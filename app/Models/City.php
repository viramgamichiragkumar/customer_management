<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    public $guarded = [];


    public function state(){
        return $this->belongsTo(State::class,'state_id','id');
    }

    public function country(){
        return $this->belongsTo(country::class,'country_id','id');
    }

    public function modifiedBy(){
        return $this->belongsTo(User::class,'modified_by','id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}

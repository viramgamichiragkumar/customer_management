<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
    public $guarded = [];

    public function modifiedBy(){
        return $this->belongsTo(User::class,'modified_by','id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $guarded = [];
    use HasFactory;

    public function country(){
        return $this->belongsTo(country::class,'country_id','id');
    }

    public function modifiedBy(){
        return $this->belongsTo(User::class,'modified_by','id');
    }

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}
